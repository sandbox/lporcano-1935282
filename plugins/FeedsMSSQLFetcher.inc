<?php

/**
 * @file
 * Home of the CMWeb Feeds Fetcher 
 */


/**
 * Result of FeedsMSSQLFetcher::fetch().
 */
class FeedsMSSQLFetcherResult extends FeedsFetcherResult {
  protected $url;
  protected $file_path;

  /**
   * Constructor.
   */
  public function __construct($host = NULL,$user = NULL,$pass = NULL,$database = NULL,$query = NULL,$rowversionfield = NULL, $feed_nid = NULL) {
    $this->host = $host;
    $this->user = $user;
    $this->pass = $pass;
    $this->database = $database;
    $this->query = $query;
    $this->feed_nid = $feed_nid;
    parent::__construct('');
  }

  /**
   * Overrides FeedsFetcherResult::getRaw();
   */
  public function getRaw() {
    feeds_mssql_feeds_include_library('mssql_build_request.inc', 'mssql_build_request');
    $result = mssql_build_request_get($this->host,$this->user,$this->pass,$this->database,$this->query,$this->feed_nid);
    // file_put_contents("/tmp/results",$result,FILE_APPEND | LOCK_EX);	
    return $this->sanitizeRaw($result);
  }
}

/**
 * Fetches data from MS Sql Server.
 */
class FeedsMSSQLFetcher extends FeedsFetcher {

  /**
   * Implements FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    $feed_nid = $source->feed_nid;
    return new FeedsMSSQLFetcherResult($source_config['host'],$source_config['username'],$source_config['password'],$source_config['database'],$source_config['query'],$feed_nid);
  }

  /**
   * Expose source form.
   */
  public function sourceForm($source_config) {
    $form = array();
    $form['host'] = array(
      '#type' => 'textfield',
      '#title' => t('Hostname'),
      '#description' => t('Enter an SQL Server Hostname.'),
      '#default_value' => isset($source_config['host']) ? $source_config['host'] : '',
      '#maxlength' => NULL,
      '#required' => TRUE,
    );
    $form['username'] = array(
      '#type' => 'textfield',
      '#title' => t('User'),
      '#description' => t('Enter an SQL Server username.'),
      '#default_value' => isset($source_config['username']) ? $source_config['username'] : '',
      '#maxlength' => NULL,
      '#required' => TRUE,
    );
    $form['password'] = array(
      '#type' => 'textfield',
      '#title' => t('PASS'),
      '#description' => t('Enter an SQL Server password.'),
      '#default_value' => isset($source_config['password']) ? $source_config['password'] : '',
      '#maxlength' => NULL,
      '#required' => TRUE,
    );
    $form['database'] = array(
      '#type' => 'textfield',
      '#title' => t('DB'),
      '#description' => t('Enter an SQL Server database.'),
      '#default_value' => isset($source_config['database']) ? $source_config['database'] : '',
      '#maxlength' => NULL,
      '#required' => TRUE,
    );
    $form['query'] = array(
      '#type' => 'textfield',
      '#title' => t('QUERY'),
      '#description' => t('Enter an SQL query.'),
      '#default_value' => isset($source_config['query']) ? $source_config['query'] : '',
      '#maxlength' => NULL,
      '#required' => TRUE,
    );
    return $form;
  }

}

