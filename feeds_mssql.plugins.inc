<?php

/**
 * @file
 * CTools plugins declarations.
 */

/**
 * Break out for feeds_feed_plugins().
 */
function _feeds_mssql_feeds_plugins() {
  $path = drupal_get_path('module', 'feeds_mssql') . '/plugins';

  $info = array();
  $info['FeedsMSSQLFetcher'] = array(
    'name' => 'Feeds MSSQL Fetcher',
    'description' => 'Fetch content from MS SQL Server.',
    'handler' => array(
      'parent' => 'FeedsFetcher', 
      'class' => 'FeedsMSSQLFetcher',
      'file' => 'FeedsMSSQLFetcher.inc',
      'path' => $path,
    ),
  );
  return $info;
}
