<?php


ini_set('mssql.charset', 'UTF-8');
ini_set ( 'mssql.textlimit' , 2147483647 );
ini_set ( 'mssql.textsize' , 2147483647 );

function mssql_build_request_get($host, $user = NULL, $pass = NULL,$db = NULL,$query = NULL,$feed_nid = NULL) {
$csv = array();

$dbhandle = mssql_connect($host, $user, $pass);
mssql_query("SET TEXTSIZE 2147483647");
$selected = mssql_select_db($db, $dbhandle);


$result = mssql_query($query);
while($row = mssql_fetch_array($result))
{
	$csv[]=$row;
}
//close the connection
mssql_close($dbhandle);
  
  return mssql_array_to_scv($csv,1);
}

function mssql_array_to_scv($array, $header_row = true, $col_sep = ",", $row_sep = "\n", $qut = '"') {
	if (!is_array($array) or !is_array($array[0])) return false;
 
		//Header row.
		if ($header_row) {
			foreach ($array[0] as $key => $val) {
				//Escaping quotes.
				$key = str_replace($qut, "$qut$qut", $key);
				$output .= "$col_sep$qut$key$qut";
			}
			$output = substr($output, 1)."\n";
		}
		//Data rows.
		foreach ($array as $key => $val) {
		$tmp = '';
		foreach ($val as $cell_key => $cell_val) {
			//Escaping quotes.
			$cell_val = str_replace($qut, "$qut$qut", $cell_val);
			$cell_val = str_replace('NULL','',$cell_val);
			
			$tmp .= "$col_sep$qut$cell_val$qut";
		}
		$rowv = substr($tmp, 1).$row_sep;
		$output .= substr($tmp, 1).$row_sep;
	}
	
	
	return $output;
}
